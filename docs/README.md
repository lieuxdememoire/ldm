# Lieux de mémoire

![Lieux de mémoire](_media/wip_ldm_player.jpg)

## Operations

### Start the installation

1. Power On the projectors
	* With Remote
	* Manualy 


2. Enable the Video players
	* On every Video player, push the button on the M5Stack Atom Lite to enable it

![m5Atom button](_media/m5atom_button.drawio.png)

### Stop the installation

1. Power Off the projectors
	* With Remote
	* Manualy 


2. Disable the Video players
	* On every Video player, push the button on the M5Stack Atom Lite to disable it
	* This will turn off image, sound and light


## Troubleshoot

### Menu bar in video player

* If a menu bar is visible on the video player screen
  * Double tap video player window via the touchscreen to restore fullscreen

![menu bar in screen](_media/menubar.drawio.png)

### Power outage

* In case of a power outage;
  * Video players will automatically boot to normal state. 
  * Projectors have to be Power ON. 


### No image on the projector

* Is projectors powered ON?
	* Power on the projector

* Is projectors powered? 
	* Check power cable on the projector
	* Check AC cable on the projector power supply

* Is the micro SD card properly seated? 
	* Check micro SD card tray and make sure it is properly seated

### No image on the Video player 

* Is the Video Player active?
	* Push the button on M5Stack Atom to change Video Player State

* Is the Video Player powered?
    * Check Ethernet switch for LED activity
	* If no blinking light on the raspberry pi 
    	* Check Ethernet connection
	* Power Cycle the Raspberry pi 
    	* Unplug and replug the Ethernet cable
  * Try another port on the switch. 


### Sound too low or too loud

* Power cycle (OFF, wait 5 sec, ON) the speaker using the power button, it should reset to normal volume
 
* If sound still not adequate, adjust sound volume on the speaker with [+] and [-] buttons

### No sound

* Is the Video Player active?
	* Push the button on M5Stack Atom

* Is the speaker active? 
	* Push power button on the speaker and make sure 
 
* Is the speaker connected? 
	* Check if auxiliary audio cable is properly connected from the video player (raspberry pi) to the speaker  

* Is the speaker powered? 
	* Check USB connection between speaker and raspberry pi

### No LED string light

* Is the Video Player connected to the LED is active?
	* Push the button on M5Stack Atom lite

* Is the LED light properly connected to the M5Stack Atom lite? 
	* Properly connect the LED light

## BoM

* 3x Video Player
	* Raspberry pi 4
	* Micro SD card
	* POE HAT
	* M5stack Atom Lite
	* Waveshare 10.1 inch DSI screen 
	* DIN Rail
	* M3 Riser
	* Ethernet cable (Slimrun cat6 pink)
	* Articulated Arm + Clamp

* 2x Projector
	* Optoma 1050st
	* Micro SD card
	* Articulated Arm + Clamp

* 1x LED String
	* 33' 100 LED fairy light
	* Grove Connector (female)
	* Grove Cable 5 cm (male-male)
  
* 1x Speaker
	* F4 audio speaker
	* Aux cable 6' (pink)
	* USB Micro to USB A 6' (pink) 
	* Articulated Arm + Clamp

* 1x AC/DC Distribution assembly
	* 1x Pink AC 25' NEMA 5-15 extention (3 plugs) 
	* DIN Rails
	* 2x AC/DC 19v 4a (projectors DC)

* 1x Telescopic pole
	* Task T74500 63" to 120" Quick Support Rod

* 1x Irridescent POE Switch
	* YuanLey 8 Port Gigabit PoE Switch 120 W
	* 2x Clamps
	* 1x 3' IEC cable 



## Schematic

![Wiring Schematic](_media/schematic.drawio.png)


## Installation

### Setup

From Top to bottom

* Deploy Telescopic Pole 
* Fasten AC/DC Distribution assembly 
* Fasten AC/DC Irridescent POE Switch
* Fasten Speaker
* Fasten Projectors
* Fasten Video Players
* Plug power on projectors
* Plug M5Stack Atom Lite on each Raspberry Pi
* Plug Ethernet cable from Irridescent Switch to Every Raspberry Pi
* Plug USB and Audio Cable from Speaker to Raspberry pi
* Plug LED Light to Video Player Groove port (USB C to C)
* Fasten LED Light to Hooks
* POWER ON 

### Dismantle

From exterior to interior

* POWER OFF
* Unplug LED 
* Remove LED from Hook, roll and fasten with velcro strap
* Unplug and store 
  * Audio and USB cable
  * Speaker
  * M5Stack atom lite 
  * Projectors
  * Video players 
   


## Photos

![Video player with LED](_media/wip_ldm_player_led.jpg)

![Video Player connected to speaker](_media/wip_ldm_player_sound.jpg)

![Irridescent Pole](_media/wip_ldm_pole.jpg)

![Speaker](_media/wip_ldm_speaker.jpg)

![Irridescent Switch](_media/wip_ldm_switch.jpg)


